package com.abdulqodir.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * Abdulqodir Ganiev 4/19/2022 12:51 AM
 */

@SpringBootApplication(
        scanBasePackages = {
                "com.abdulqodir.customer",
                "com.abdulqodir.amqp",
        }
)
@EnableEurekaClient
@EnableFeignClients(
        basePackages = "com.abdulqodir.clients"
)
//@PropertySources({
//        @PropertySource("classpath:clients-${spring.profiles.active}.properties")
//})
public class CustomerApplication {
    public static void main(String[] args) {
        SpringApplication.run(CustomerApplication.class, args);
    }
}
