package com.abdulqodir.customer;

public record CustomerRegistrationRequest(
        String firstName,
        String lastName,
        String email) {
}
