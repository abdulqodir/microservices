package com.abdulqodir.book;

import com.abdulqodir.clients.book.BookResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import java.util.List;

/**
 * Abdulqodir Ganiev 4/25/2022 9:16 AM
 */

@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;

    public List<Book> getBooks() {
        return null;
    }

    public String add(BookDto dto) {

        // TODO: 4/25/2022 make checks

        Book book = Book
                .builder()
                .name(dto.getName())
                .author(dto.getAuthor())
                .build();

        bookRepository.save(book);

        return "Success";

    }

    public BookResponse get(Integer id) {
        try {
            Book book = bookRepository.findById(id)
                    .orElseThrow(() -> new ResourceAccessException("NOT FOUND"));

            return BookResponse
                    .builder()
                    .id(book.getId())
                    .name(book.getName())
                    .author(book.getAuthor())
                    .build();

        } catch (Exception e) {
            return null;
        }
    }
}
