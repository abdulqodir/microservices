package com.abdulqodir.book;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Abdulqodir Ganiev 4/25/2022 9:16 AM
 */

public interface BookRepository extends JpaRepository<Book, Integer> {
}
