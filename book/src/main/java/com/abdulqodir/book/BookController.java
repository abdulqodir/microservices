package com.abdulqodir.book;

import com.abdulqodir.clients.book.BookResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Abdulqodir Ganiev 4/25/2022 9:16 AM
 */

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/book")
public class BookController {

    private final BookService bookService;

    @PostMapping
    public String addNewBook(@RequestBody BookDto dto) {
        return bookService.add(dto);
    }

    @GetMapping
    public List<Book> getAllBooks() {
        return bookService.getBooks();
    }

    @GetMapping("/{id}")
    public BookResponse getSingleBook(@PathVariable Integer id) {
        return bookService.get(id);
    }
}
