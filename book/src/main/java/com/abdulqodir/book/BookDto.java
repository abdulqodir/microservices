package com.abdulqodir.book;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * Abdulqodir Ganiev 4/25/2022 9:44 AM
 */

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookDto {

    String name;

    String author;
}
