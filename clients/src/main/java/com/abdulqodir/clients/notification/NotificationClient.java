package com.abdulqodir.clients.notification;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Abdulqodir Ganiev 4/22/2022 9:04 AM
 */

@FeignClient("notification")
public interface NotificationClient {

    @PostMapping(path = "api/v1/notification")
    void sentNotification(NotificationRequest request);
}
