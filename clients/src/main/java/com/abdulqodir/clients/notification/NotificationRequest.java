package com.abdulqodir.clients.notification;

/**
 * Abdulqodir Ganiev 4/22/2022 9:06 AM
 */

public record NotificationRequest(Integer toCustomerId,
                                  String toCustomerEmail,
                                  String message) {
}
