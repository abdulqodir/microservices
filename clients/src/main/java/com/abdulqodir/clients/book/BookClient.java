package com.abdulqodir.clients.book;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("book")
public interface BookClient {

    @GetMapping("api/v1/book/{id}")
    BookResponse getSingleBook(@PathVariable("id") Integer id);
}
