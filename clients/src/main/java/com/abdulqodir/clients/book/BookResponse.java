package com.abdulqodir.clients.book;

import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * Abdulqodir Ganiev 4/25/2022 10:34 AM
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookResponse {
    Integer id;
    String name;
    String author;
}
