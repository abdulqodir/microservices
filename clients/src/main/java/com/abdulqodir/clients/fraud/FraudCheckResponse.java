package com.abdulqodir.clients.fraud;

/**
 * Abdulqodir Ganiev 4/22/2022 7:06 AM
 */

public record FraudCheckResponse(Boolean isFraudster) {
}
