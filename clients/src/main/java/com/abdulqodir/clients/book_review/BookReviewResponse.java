package com.abdulqodir.clients.book_review;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookReviewResponse {
    String name;
    String author;
    List<BookReviewView> reviews;
}
