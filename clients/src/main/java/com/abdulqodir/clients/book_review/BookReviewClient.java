package com.abdulqodir.clients.book_review;

import org.springframework.cloud.openfeign.FeignClient;

/**
 * Abdulqodir Ganiev 4/25/2022 10:54 AM
 */

@FeignClient("book-review")
public interface BookReviewClient {
}
