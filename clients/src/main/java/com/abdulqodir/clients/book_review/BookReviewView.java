package com.abdulqodir.clients.book_review;

public interface BookReviewView {
    String getReview();

    Integer getFromUserId();
}
