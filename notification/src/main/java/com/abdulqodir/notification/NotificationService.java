package com.abdulqodir.notification;

import com.abdulqodir.clients.notification.NotificationRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Abdulqodir Ganiev 4/22/2022 9:03 AM
 */

@Slf4j
@Service
@AllArgsConstructor
public class NotificationService {

    private final NotificationRepository notificationRepository;
    private final EmailService emailService;

    public void send(NotificationRequest request) {

        Notification notification = Notification
                .builder()
                .toCustomerId(request.toCustomerId())
                .toCustomerEmail(request.toCustomerEmail())
                .message(request.message())
                .sender("Abdulqodir")
                .sentAt(LocalDateTime.now())
                .build();

        log.info("new notification {}", notification);

        notificationRepository.save(notification);

        emailService.sendEmail(notification);

    }
}
