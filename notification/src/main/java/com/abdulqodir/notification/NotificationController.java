package com.abdulqodir.notification;

import com.abdulqodir.clients.notification.NotificationRequest;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Abdulqodir Ganiev 4/22/2022 8:55 AM
 */

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/notification")
public class NotificationController {

    private final NotificationService notificationService;

    @PostMapping
    public void sendNotification(
            @RequestBody NotificationRequest request){
        notificationService.send(request);
    }
}
