package com.abdulqodir.notification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Abdulqodir Ganiev 4/22/2022 8:51 AM
 */

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(
        basePackages = "com.abdulqodir.clients"
)
public class NotificationApplication {
    public static void main(String[] args) {
        SpringApplication.run(NotificationApplication.class, args);
    }
}
