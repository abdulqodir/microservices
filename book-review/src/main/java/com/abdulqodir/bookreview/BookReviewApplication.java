package com.abdulqodir.bookreview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Abdulqodir Ganiev 4/25/2022 9:25 AM
 */

@SpringBootApplication()
@EnableEurekaClient
@EnableFeignClients(
        basePackages = "com.abdulqodir.clients"
)
public class BookReviewApplication {
    public static void main(String[] args) {
        SpringApplication.run(BookReviewApplication.class, args);
    }
}
