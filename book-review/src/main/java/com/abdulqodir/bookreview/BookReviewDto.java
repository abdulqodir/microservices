package com.abdulqodir.bookreview;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * Abdulqodir Ganiev 4/25/2022 9:54 AM
 */

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookReviewDto {
    String review;
    Integer bookId;
    Integer fromUserId;
}
