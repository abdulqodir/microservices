package com.abdulqodir.bookreview;

import com.abdulqodir.clients.book_review.BookReviewResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * Abdulqodir Ganiev 4/25/2022 9:26 AM
 */

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/book-review")
public class BookReviewController {

    private final BookReviewService bookReviewService;

    @PostMapping
    public String addNewBookReview(@RequestBody BookReviewDto dto) {
        return bookReviewService.add(dto);
    }

    @GetMapping("/get-book-review/{id}")
    public BookReviewResponse getBookReviews(@PathVariable Integer id){
        return bookReviewService.getSingleBookReviews(id);
    }
}
