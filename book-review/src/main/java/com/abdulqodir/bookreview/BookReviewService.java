package com.abdulqodir.bookreview;

import com.abdulqodir.clients.book.BookClient;
import com.abdulqodir.clients.book.BookResponse;
import com.abdulqodir.clients.book_review.BookReviewResponse;
import com.abdulqodir.clients.book_review.BookReviewView;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Abdulqodir Ganiev 4/25/2022 9:34 AM
 */
@Service
@RequiredArgsConstructor
public class BookReviewService {

    private final BookReviewRepository bookReviewRepository;
    private final BookClient bookClient;
    private final RabbitTemplate rabbitTemplate;

    public String add(BookReviewDto dto) {

        bookClient.getSingleBook(dto.getBookId());

        BookReview bookReview = BookReview
                .builder()
                .review(dto.getReview())
                .fromUserId(dto.getFromUserId())
                .bookId(dto.getBookId())
                .build();

        bookReviewRepository.save(bookReview);
        return "Success";
    }

    public BookReviewResponse getSingleBookReviews(Integer bookId) {

        BookResponse book = bookClient.getSingleBook(bookId);

        List<BookReviewView> bookReviews = bookReviewRepository.getAllReviewByBookId(book.getId());

        return BookReviewResponse
                .builder()
                .name(book.getName())
                .author(book.getAuthor())
                .reviews(bookReviews)
                .build();


    }
}
