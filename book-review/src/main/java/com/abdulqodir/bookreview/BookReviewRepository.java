package com.abdulqodir.bookreview;

import com.abdulqodir.clients.book_review.BookReviewView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookReviewRepository extends JpaRepository<BookReview, Integer> {
    @Query(
            nativeQuery = true,
            value = "select from_user_id as fromUserId, review from book_review where book_id = :bookId"
    )
    List<BookReviewView> getAllReviewByBookId(@Param("bookId") Integer bookId);
}
