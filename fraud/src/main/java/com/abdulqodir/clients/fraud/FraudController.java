package com.abdulqodir.clients.fraud;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/fraud-check")
public class FraudController {

    private final FraudCheckService fraudCheckService;

    @GetMapping("/{customerId}")
    public FraudCheckResponse isFraudster(
            @PathVariable("customerId") Integer customerId) {

        boolean fraudulentCustomer = fraudCheckService
                .isFraudulentCustomer(customerId);

        return new FraudCheckResponse(fraudulentCustomer);
    }
}
